# KA_2_React

Creating nested components, props, keys

Основные материалы:
https://ru.reactjs.org/docs/components-and-props.html
https://ru.reactjs.org/docs/state-and-lifecycle.html
https://ru.reactjs.org/docs/lists-and-keys.html
https://ru.reactjs.org/docs/lifting-state-up.html
Дополнительные материалы:
https://habr.com/ru/company/ruvds/blog/434118/
https://ru.reactjs.org/docs/composition-vs-inheritance.html
https://medium.com/@jmuse/%D0%BF%D0%B5%D1%80%D0%B5%D0%B4%D0%B0%D1%87%D0%B0-%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85-%D0%BC%D0%B5%D0%B6%D0%B4%D1%83-%D0%BA%D0%BE%D0%BC%D0%BF%D0%BE%D0%BD%D0%B5%D0%BD%D1%82%D0%B0%D0%BC%D0%B8-%D0%B2-react-d86394da2b50
https://medium.com/@kanby/%D0%BF%D1%80%D0%B5%D0%B7%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D0%BE%D0%BD%D0%BD%D1%8B%D0%B9-%D0%BA%D0%BE%D0%BC%D0%BF%D0%BE%D0%BD%D0%B5%D0%BD%D1%82-%D0%B8-%D0%BA%D0%BE%D0%BD%D1%82%D0%B5%D0%B9%D0%BD%D0%B5%D1%80-%D0%B2-react-f0e118480809
https://habr.com/ru/company/hh/blog/352150/
Видео:
https://learn.javascript.ru/screencast/react - видео 7-8, 11-12
https://www.youtube.com/watch?v=tn9HyYRVZ9A
